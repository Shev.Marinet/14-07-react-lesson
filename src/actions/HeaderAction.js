export const SET_NAME = "SET_NAME";
export const SET_PASSWORD = "SET_PASSWORD";
export const PASS_REQUEST = "PASS_REQUEST";


export function setName(name) {
    return {
        type: SET_NAME,
        payload : name
    }
}
export function requestPass(password) {
    return dispatch => {
        dispatch ({
            type : PASS_REQUEST,
            payload: 'wait for password ...'
        });

        setTimeout(() => {
            fetch(`https://swapi.co/api/people/${parseInt(Math.random()*87)}/`)
                .then((res) => {
                    return res.json();
                })
                .then((data) => dispatch({
                    type: SET_PASSWORD,
                    payload : data.name
                }))
            // dispatch({
            //     type: SET_PASSWORD,
            //     payload : password.toString().split('').reverse().join('')
            // })
        },1000)

    }
}
export function setPassword(pass) {
    return {
        type: SET_PASSWORD,
        payload: pass
    }
}
