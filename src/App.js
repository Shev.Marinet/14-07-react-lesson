import React, {Component, Fragment} from 'react';
import {Route} from 'react-router-dom';
import { connect } from 'react-redux';
import {SubmissionError} from 'redux-form';

import logo from './logo.svg';

import Welcome from './components/Welcome/Welcome';
import Button from './components/Button/Button';
import Header from './components/Header/Header';
import MainSection from './components/MainSection/MainSection';
import Footer from './components/Footer/Footer';
import List from './components/List/List';
import LoginForm from "./components/Form/Form.js";
import {setName, setPassword, requestPass} from "./actions/HeaderAction";

import './App.css';


const comment = {
    date: new Date(),
    text: 'I hope you enjoy learning React!',
    author: {
        name: 'Hello Kitty',
        avatarUrl: 'http://placekitten.com/g/64/64'
    }
};
// function Welcome(props) {
//   return <h1>Hello, {props.name}</h1>;
// }

// function App() {
//   return (
//       <div>
//         <Welcome name="Sara" color = "purple" lastName = '1111'  font = '111'/>
//         {/*<Welcome name="Cahal" color = "green" lastName = '2222'  font = '222'/>*/}
//         {/*<Welcome name="Edite" color = "blue" lastName = '3333'  font = '333'/>*/}
//         <Button onClick={()=>{console.log("gfhbghn")}}/>
//       </div>
//   );
// }
// function App() {
//   return <Comment
//       date={comment.date}
//       text={comment.text}
//       author={comment.author}
//   />
// }
// function App() {
//   const user = {
//     firstName : "Marina",
//     lastName : "Shevchuk"
//   };
//   const element  = <h1>Hello, {formatName(user)}</h1>;
//   function formatName(user) {
//     return user.firstName + " " + user.lastName;
//   }
//   return (
//       element
//   );
// }

function Avatar(props) {
    return (
        <img className="Avatar"
             src={props.user.avatarUrl}
             alt={props.user.name}/>
    );
}

function UserInfo(props) {
    return (
        <div className="UserInfo">
            <Avatar user={props.user} />
            <div className="UserInfo-name">
                {props.user.name}
            </div>
        </div>
    );
}

function formatDate(date) {
    return date.toLocaleDateString();
}

function Comment (props) {
    return (
        <div className="Comment">
            <UserInfo user={props.author} />
            <div className="Comment-text">
                {props.text}
            </div>
            <div className="Comment-date">
                {formatDate(props.date)}
            </div>
        </div>
    );
}

// class App extends Component{
//     constructor (props) {
//         super (props);
//         this.state = {
//             newText: false
//         };
//         this.changeText = this.changeText.bind(this);
//     }
//
//     render(){
//         return (
//             <div>
//                 <Header text = {this.state.newText}/>
//                 <MainSection callback1 = {this.changeText} />
//                 <Footer />
//             </div>
//
//         );
//     }
//     changeText(){
//         this.setState({newText: !this.state.newText})
//         // console.log(('working'));
//     }
// }


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newText: 'false'
        };
        this.changeText = this.changeText.bind(this);
    }

//     componentDidUpdate() {
// //         this.changeText();
//
// }
    createNewPass = () => parseInt(Math.random()*11111);
    render(){
        const {user, footer} = this.props;
        return (
            <Fragment>
                {/*<Header text = {this.state.newText} />*/}

                <Header
                    text={`Name: ${user.name}, pass: ${user.password}, age: ${user.age} footer: ${footer.projectName}`}
                    callback1 ={() => {
                        this.props.setNameAction('Ira');
                    }}
                    callback2 ={() => {
                        // this.props.setPassAction (this.createNewPass());
                        this.props.requestPassAction (this.createNewPass());
                    }}
                />

                {/*<MainSection callback1 = {this.changeText} />*/}
                <List />
                {/*<Footer />*/}
                <Route path="/header" component={Header} />
                <Route path="/mainsection" component={MainSection} />
                <Route path="/footer" component={Footer} />
                <h1>Form</h1>
                <LoginForm onSubmit={this.submit}
                           initialValues={this.getInitialValues()}/>
            </Fragment>


        );
    }
    changeText(){
        this.setState({newText: !this.state.newText})
        // console.log(('working'));
    }
    submit = input=>{
        if(['Настя', 'Леша', 'Миша', 'Света'].includes (input.username)){
            throw new SubmissionError ({
                username : "Имя пользователя уже существует",
            });
        }else{
            window.alert (JSON.stringify(input))
        }
    };
    getInitialValues () {
        return {
            username: 'Юрчик',
            password: '',
        };
    }
}

const mapStateToProps = function (store) {
    console.log('string',store);
    return {
        user: store.user.client,
        footer : store.footer.info
    }
};

const mapDispatchToProps = function (dispatch) {
    return {
        setNameAction :(name) => dispatch(setName(name)),
        // setPassAction : (pass) => dispatch (setPassword(pass)),
        requestPassAction : (pass) => dispatch (requestPass(pass))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
// export default App;
