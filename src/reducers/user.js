import {SET_NAME, SET_PASSWORD, PASS_REQUEST} from '../actions/HeaderAction';

const initialState = {
    client: {
        name: "Ann",
        password: "123456",
        age: 27
    }
};

export function userReducer(state = initialState, action) {
    switch (action.type) {
        case SET_NAME:
            return {...state, client: {...state.client, name: action.payload}};
        case SET_PASSWORD :
            return {...state, client: {...state.client, password: action.payload}};
        case PASS_REQUEST :
            return {...state, client: {...state.client, password: action.payload}};
        default:
            return state;
    }
}
