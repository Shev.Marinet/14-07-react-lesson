import { combineReducers } from 'redux';
import { footerReducer } from './footer';
import { userReducer } from './user';
import {reducer as formReducer} from 'redux-form';

export const rootReducer= combineReducers({
    footer: footerReducer,
    user: userReducer,
    form: formReducer
    }
);
