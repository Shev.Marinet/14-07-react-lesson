import React, { Component } from 'react';
import './Welcome.css'


// function Welcome(props) {
//     return <h1 className ={props.color}>Hello, {props.name}</h1>;
// }


class Welcome extends Component {
    constructor (props) {
        super (props);
        this.state = {newstate: ''};
    }
    componentDidMount() {
        this.ChangeProps();
    }
    ChangeProps () {
        const {name, color} = this.props;
        console.log(name);
        return this.setState({newstate: `${name+color}`});
    }

    render () {
        const {name, color} = this.state;
        console.log(this.state.newstate);
        // console.warn(this.ChangeProps(this.state.newstate));
        return <h1 className ={color}>Hello, {name}   {this.state.newstate}
        </h1>;
    }
}
export  default  Welcome;