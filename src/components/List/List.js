import  React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './List.css'

class List extends Component {
    constructor(props) {
        super (props)
    }
    render() {

        return (
            <ul className = "List">
                <li>
                    <Link  to='/header' className = "list">
                        Header
                    </Link>
                </li>
                <li>
                    <Link to='/mainsection' className = "list">
                        Main Section
                    </Link>
                </li>
                <li>
                    <Link to='/footer' className = "list">
                        Footer
                    </Link>
                </li>
            </ul>
        )
    }
}

export  default  List;
