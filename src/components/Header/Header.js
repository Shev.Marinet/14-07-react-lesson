import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
    static defaultProps = {
        text: "default"
    };
    render () {
        return (
        <Fragment>
            <h1>{this.props.text}</h1>
            {
                this.props.text ?
                    <p>Let be peace</p> :
                    <p>Changed</p>
            }
            <button onClick={this.props.callback1}>Change name</button>
            <button onClick={this.props.callback2}>Change pass</button>
        </Fragment>)
            }
}

Header.propTypes = {
    text: PropTypes.bool
};

export default Header;
